Rails.application.routes.draw do
  root "votes#new"

  resources :votes, only: [:create]

  resource :audit_votes, only: [:new, :show, :create]

  resources :audit_votes_auth, only: [:create]
end
