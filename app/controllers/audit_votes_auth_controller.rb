class AuditVotesAuthController < ApplicationController
  def create
    if log_in(params[:code])
      flash.notice = "Autorizado com sucesso"
      redirect_to audit_votes_path
    else
      flash.alert = "Você não está autorizado"
      redirect_to new_audit_votes_path
    end
  end
end