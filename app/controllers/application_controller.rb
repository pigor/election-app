class ApplicationController < ActionController::Base
  include SessionsHelper
  
  private
  
  def logged_in_user
      unless logged_in?
        flash[:danger] = "Você não tem permissão"
        redirect_to new_audit_votes_path
      end
  end
end
