class VotesController < ApplicationController
  def new
    @vote = Vote.new
  end

  def create
    vote_params = params.require(:vote).permit(:candidate_code)
    vote = Vote.new vote_params
    
    if vote.save 
      flash.notice = "Voto computado com sucesso!"
      redirect_to root_path
    else
      flash.alert = "Seu voto não foi computado, procure o fiscal responsável!"
      redirect_to root_path
    end
  end
end
