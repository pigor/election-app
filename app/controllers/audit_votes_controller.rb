class AuditVotesController < ApplicationController
  before_action :logged_in_user, only: [:show, :create]

  def new
  end

  def show
  end

  def create
    AuditVotes.generate_audit_file

    @file = File.open(AuditVotes::AUDIT_FILE, "r")

    send_data @file.read, filename: File.basename(AuditVotes::AUDIT_FILE)
  end
end