class AuditVotes
  AUDIT_FILE = "/tmp/audit_election.txt"
  
  class << self
    def all
      votes = Vote.all
      votes.map(&:candidate_code).push("@").join(" ")
    end
  
    def generate_audit_file
      votes = all

      File.write(AuditVotes::AUDIT_FILE, votes)
    end
  end
end