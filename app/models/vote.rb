class Vote < ApplicationRecord
  validates :candidate_code, presence: true, 
                             format: { with: /\A[0-9]+\z/, message: "voto inválido, usar apenas números"}
end
