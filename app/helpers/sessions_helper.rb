module SessionsHelper
  ACCESS_CODE = "XPTO"

  def log_in(code)
    session[:audit_user] = true if code == ACCESS_CODE

    session[:audit_user]
  end

  def log_out
    session.delete(:audit_user)
  end

  def logged_in?
    !session[:audit_user].nil?
  end
end