require 'test_helper'

class AuditVotesControllerTest < ActionDispatch::IntegrationTest
  test "restrict access blocked" do
    get "/audit_votes"
    assert_response :redirect
    follow_redirect!

    assert_response :success
    assert_select "div", "Você não tem permissão"
  end

  test "restrict access allowed" do
    post "/audit_votes_auth", params: {code: "XPTO"}
    assert_response :redirect
    follow_redirect!

    assert_response :success
    assert_select "div", "Autorizado com sucesso"
  end
end
