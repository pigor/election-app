require 'test_helper'

class VotesControllerTest < ActionDispatch::IntegrationTest
  test "register vote" do
    post "/votes", params: { vote: { candidate_code: "10" } }
    assert_response :redirect
    follow_redirect!

    assert_response :success
    assert_select "div", "Voto computado com sucesso!"
  end
end
