require 'test_helper'

class AuditVotesTest < ActiveSupport::TestCase
  test "#all" do
    5.times.each do |num|
      Vote.create candidate_code: "10"
    end

    assert_equal "10 10 10 10 10 @", AuditVotes.all
  end

  test "#generate_audit_file" do
    5.times.each do |num|
      Vote.create candidate_code: "10"
    end

    AuditVotes.generate_audit_file

    file_content = File.read('/tmp/audit_election.txt')

    assert_equal "10 10 10 10 10 @", file_content 
  end
end
