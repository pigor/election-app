require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  test "register vote" do
    vote = Vote.new candidate_code: "10"

    assert vote.save
  end

  test "register vote using alphanumeric" do
    vote = Vote.new candidate_code: "AA"
    
    assert_not vote.save
  end
end
